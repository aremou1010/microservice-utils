/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceutils.controller.web;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import org.agrosfer.pdm.microserviceutils.helpers.PassGen;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author g3a
 */
@RestController
public class UtilsController {
    
    @PostMapping("/utils/password/encrypt")
    public ResponseEntity<?> encrypt(@RequestBody String password){
        
        //String salt = getSalt();
        String salt = Timestamp.from(Instant.now()).getTime()+"gr455|4bj`kg"+Timestamp.from(Instant.now()).getTime()+"g@_pdm-(*/zekjl"+Timestamp.from(Instant.now()).getTime();
        
        System.out.println("Password : "+password); //Prints 83ee5baeea20b6c21635e4ea67847f66
        String securePassword = getSecurePassword(password, salt);
        System.out.println("securePassword : "+securePassword); //Prints 83ee5baeea20b6c21635e4ea67847f66
        System.out.println("salt : "+salt); //Prints 83ee5baeea20b6c21635e4ea67847f66
        //System.out.println("salt to string: "+Arrays.toString(salt)); //Prints 83ee5baeea20b6c21635e4ea67847f66
        
        PassGen passGen = new PassGen(securePassword, salt);
//        PassGen passGen = null;
//        passGen.setSecurePassword(securePassword);
//        passGen.setSalt(salt);
        
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(passGen);
        
    }
    
    @PostMapping("/utils/password/decrypt")
    public String decrypt(@RequestBody PassGen input){
        
        String securePassword = getSecurePassword(input.getSecurePassword(), input.getSalt());
        
        return securePassword;
    }
    
    private static String getSalt() throws NoSuchAlgorithmException, NoSuchProviderException
    {
        //Always use a SecureRandom generator
//        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
        SecureRandom sr = SecureRandom.getInstanceStrong();
        //Create array for salt
        byte[] salt = new byte[16];
        //Get a random salt
        sr.nextBytes(salt);
        //return salt
        return salt.toString();
    }
    
    private static String getSecurePassword(String passwordToHash, String salt)
    {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            byte[] b = salt.getBytes();
            md.update(b);
            //Get the hash's bytes 
            byte[] bytes = md.digest(passwordToHash.getBytes());
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
    
}
