/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceutils.helpers;

import lombok.Data;

/**
 *
 * @author g3a
 */
@Data
public class PassGen {
    
    String securePassword;
    
    String salt;

    public PassGen() {
    }

    public PassGen(String securePassword, String salt) {
        this.securePassword = securePassword;
        this.salt = salt;
    }
    
}
